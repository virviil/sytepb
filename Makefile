protoc_middleman:
	rm -rf sytepb/*.{py,h,cc,c}
	protoc -I=./ --cpp_out=./ sytepb/*.proto
	protoc -I=./ --python_out=./ sytepb/*.proto
	python3 -m grpc_tools.protoc -I=./ --python_out=. --grpc_python_out=. sytepb/*.proto
	protoc-c -I=./ --c_out=./ sytepb/*.proto
	@touch protoc_middleman


rust:
	@pb-rs --include $(shell pwd) -d src sytepb/*.proto

golang:
	protoc -I=./ --go_out=./ --go_opt=module=gitlab.com/syte.ai/backend/sytepb --go-grpc_out=./ --go-grpc_opt=module=gitlab.com/syte.ai/backend/sytepb sytepb/*.proto
	

erlang:
	@DEBUG=1 rebar3 compile

clean:
	@rm -rf sytepb/*.{py,h,cc,c,go}
	@rm -f protoc_middleman
	@rm -f include/sytepb_*
	@rm -f $(shell find src -name "*.erl")
	@rm -f $(shell find src/sytepb -name "*.rs")

.PHONY: clean
