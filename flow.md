```mermaid
graph TD
  subgraph Ingestion
    i1[(Download and preprocess feed)]
    i2[/"partition feed to ~10.000 rows"/]
    i3[/run a feeder\]
    i1-->i2-->i3
  end

  subgraph "[Map]Feeder (run for each row in 10.000 bulk)"
    i3-->f1
    f1[("Download image")]
    f2[/"Run tagit (get `gender` and `category` for image"/]
    f3{{"Get image bounds (one image can contain bounds for different categories)"}}
    
    f4{"Match categorie with bounds & select relevant"}
    f6{{"Query vectorizer: > VectorizeRequest : < VectorizeResponse"}}
    f7[/"Generate vector_id"/]
    f8>"Write `Ad` to `*.pb` file"]
    f9{{"Write to ElasticSearch"}}
    f10{New feed or feed diff?}
    f1-->f3
    f1-->f2
    f3-->f4
    f2-->f4-->f6-->f7
    f7-->f8
    f7-->f9
    f7-->f10
  end

  subgraph "[Reduce]Clustering"
    f10 -- New feed -->c1
    c1-->c2-->c2e & c2v-->c3
    c1[/"Run clustering (k-means)"/]
    c2[\"Collect centroids"/]
    c2e{{"Write centroids to ElasticSearch"}}
    c2v{{"Write centroids to VDB"}}
    c3[/"Iterate all products in ElasticSearch"/]
    c31["Update product's vector ids"]
    c3-->c31-->c3
    c31-.-f9
  end

  subgraph "[Reduce]Protobuf diff"
    f10 -- Feed diff -->d1
    d1{{"Calculate diffs"}}
    d1p[/"Delete list of vectors from `*.pb`"/]
    d2p[/"Insert list of vectors to `*.pb`"/]
    d1v[/"Delete list of vectors from VDB"/]
    d2v[/"Insert list of vectors to VDB"/]
    d3{{"Update vectors centroids in ElasticSearch"}}
    d1-->d1p-->d2p-->d3
    d1-->d1v-->d2v-->d3
    c2e-.-d3
  end

  z1[/"Zip PB and upload to cloud storage"/]
  d3 & c3-->z1
  f8-.-z1
```
