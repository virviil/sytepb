# protorepo - one repo to rule them all


## General Protobuf messages domain

### Description

### Diagram

```mermaid
classDiagram
  class Vector {
    + [float] signature
    + [float] color
    - int32 network_version
  }
  class Attribute {
    + string name
    + string value
    + float name_proto
    + float value_prob
  }
  class Bound {
    + float x0
    + float y0
    + float x1
    + float y1
    - string label
  }
	class Ad {
    - string id
    - string index_name
    + bytes|encoded Vector| vector
    + [Attribute] attributes
    - string sig_md5
    - string csv_id
    - string vector_id
    - bytes metadata
  }
  Ad <|-- Vector
  Ad <|-- Attribute
  class CBIRResult {
    + bytes|encoded Vector| vector
    + [Attribute] attributes
  }
  CBIRResult <|-- Attribute
  CBIRResult <|-- Vector
  class VectorizeRequest {
    + string request_id
    + bytes image_bin
    + [Bound] bounds_list
    - string account_id
  }
  VectorizeRequest <|-- Bound
  class VectorizeResponse {
    + string request_id
    + [CBIRResult] results
  }
  VectorizeResponse <|-- CBIRResult
  class IdDistance {
    + string id
    - string index
    - float distance
  }
  class DistanceQuery {
    + bytes|encoded Vector| vector
    + [IdDistance] index_id
    - float color_weight
  }
  DistanceQuery <|-- IdDistance
  DistanceQuery <|-- Vector
  class DistanceReply {
    + [IdDistance] distances
  }
  DistanceReply <|-- IdDistance
```

**Legend**:

* `-` - field is **optional**
* `+` - field is **required**
* `<-` - describes relations between proto messages

## Feed indexing process

```mermaid
sequenceDiagram
  participant F as feeder
  participant B as bounds
  participant V as vectorizer
  participant ES as ElasticSearch
  participant VDB
  participant FS as FileSystem

  Note over F, B: Retrieving bounds
  F->>+B: RAW binary image representation 
  B->>-F: Json with bounds data

  F-->F: Repacking json response into `VectorizeRequest` protobuf

  Note over F, V: Retrieving vectors
  F->>+V: pb: VectorizeRequest
  V->>-F: pb: VectorizeResponse

  F-->F: Unpacking `VectorizeResponse` protobuf into CBIRResults list

  Note over F, ES: Indexing data into ElasticSearch
  F->>ES: create document in index

  Note over F, FS: Storing `*.pb` files
  F->>FS: create first iteration `*.pb` files
```

## Matching process


# Building

## Erlang

* Run `rebar3 protobuf compile`
* Commit changes

## Rust

```
pb-rs --include /Users/virviil/syte/sytepb -d src sytepb/*.proto
```