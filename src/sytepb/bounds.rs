// Automatically generated rust module for 'bounds.proto' file

#![allow(non_snake_case)]
#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(unused_imports)]
#![allow(unknown_lints)]
#![allow(clippy::all)]
#![cfg_attr(rustfmt, rustfmt_skip)]


use std::borrow::Cow;
use quick_protobuf::{MessageRead, MessageWrite, BytesReader, Writer, WriterBackend, Result};
use quick_protobuf::sizeofs::*;
use super::super::*;

#[derive(Debug, Default, PartialEq, Clone)]
pub struct Box_pb<'a> {
    pub label: Vec<Cow<'a, str>>,
    pub probability: f32,
    pub x0: f32,
    pub y0: f32,
    pub x1: f32,
    pub y1: f32,
    pub scaled_x0: f32,
    pub scaled_y0: f32,
    pub scaled_x1: f32,
    pub scaled_y1: f32,
}

impl<'a> MessageRead<'a> for Box_pb<'a> {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(10) => msg.label.push(r.read_string(bytes).map(Cow::Borrowed)?),
                Ok(21) => msg.probability = r.read_float(bytes)?,
                Ok(29) => msg.x0 = r.read_float(bytes)?,
                Ok(37) => msg.y0 = r.read_float(bytes)?,
                Ok(45) => msg.x1 = r.read_float(bytes)?,
                Ok(53) => msg.y1 = r.read_float(bytes)?,
                Ok(61) => msg.scaled_x0 = r.read_float(bytes)?,
                Ok(69) => msg.scaled_y0 = r.read_float(bytes)?,
                Ok(77) => msg.scaled_x1 = r.read_float(bytes)?,
                Ok(85) => msg.scaled_y1 = r.read_float(bytes)?,
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl<'a> MessageWrite for Box_pb<'a> {
    fn get_size(&self) -> usize {
        0
        + self.label.iter().map(|s| 1 + sizeof_len((s).len())).sum::<usize>()
        + 1 + 4
        + 1 + 4
        + 1 + 4
        + 1 + 4
        + 1 + 4
        + 1 + 4
        + 1 + 4
        + 1 + 4
        + 1 + 4
    }

    fn write_message<W: WriterBackend>(&self, w: &mut Writer<W>) -> Result<()> {
        for s in &self.label { w.write_with_tag(10, |w| w.write_string(&**s))?; }
        w.write_with_tag(21, |w| w.write_float(*&self.probability))?;
        w.write_with_tag(29, |w| w.write_float(*&self.x0))?;
        w.write_with_tag(37, |w| w.write_float(*&self.y0))?;
        w.write_with_tag(45, |w| w.write_float(*&self.x1))?;
        w.write_with_tag(53, |w| w.write_float(*&self.y1))?;
        w.write_with_tag(61, |w| w.write_float(*&self.scaled_x0))?;
        w.write_with_tag(69, |w| w.write_float(*&self.scaled_y0))?;
        w.write_with_tag(77, |w| w.write_float(*&self.scaled_x1))?;
        w.write_with_tag(85, |w| w.write_float(*&self.scaled_y1))?;
        Ok(())
    }
}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct Bounds<'a> {
    pub boxes: Vec<sytepb::bounds::Box_pb<'a>>,
}

impl<'a> MessageRead<'a> for Bounds<'a> {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(10) => msg.boxes.push(r.read_message::<sytepb::bounds::Box_pb>(bytes)?),
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl<'a> MessageWrite for Bounds<'a> {
    fn get_size(&self) -> usize {
        0
        + self.boxes.iter().map(|s| 1 + sizeof_len((s).get_size())).sum::<usize>()
    }

    fn write_message<W: WriterBackend>(&self, w: &mut Writer<W>) -> Result<()> {
        for s in &self.boxes { w.write_with_tag(10, |w| w.write_message(s))?; }
        Ok(())
    }
}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct BatchBounds<'a> {
    pub bounds: sytepb::bounds::Bounds<'a>,
}

impl<'a> MessageRead<'a> for BatchBounds<'a> {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(10) => msg.bounds = r.read_message::<sytepb::bounds::Bounds>(bytes)?,
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl<'a> MessageWrite for BatchBounds<'a> {
    fn get_size(&self) -> usize {
        0
        + 1 + sizeof_len((&self.bounds).get_size())
    }

    fn write_message<W: WriterBackend>(&self, w: &mut Writer<W>) -> Result<()> {
        w.write_with_tag(10, |w| w.write_message(&self.bounds))?;
        Ok(())
    }
}

