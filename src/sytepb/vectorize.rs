// Automatically generated rust module for 'vectorize.proto' file

#![allow(non_snake_case)]
#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(unused_imports)]
#![allow(unknown_lints)]
#![allow(clippy::all)]
#![cfg_attr(rustfmt, rustfmt_skip)]


use std::borrow::Cow;
use quick_protobuf::{MessageRead, MessageWrite, BytesReader, Writer, WriterBackend, Result};
use quick_protobuf::sizeofs::*;
use super::super::*;

#[derive(Debug, Default, PartialEq, Clone)]
pub struct VectorizeRequest<'a> {
    pub request_id: Cow<'a, str>,
    pub image_bin: Cow<'a, [u8]>,
    pub bounds_list: Vec<sytepb::common::Bounds<'a>>,
    pub account_id: Option<Cow<'a, str>>,
}

impl<'a> MessageRead<'a> for VectorizeRequest<'a> {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(10) => msg.request_id = r.read_string(bytes).map(Cow::Borrowed)?,
                Ok(18) => msg.image_bin = r.read_bytes(bytes).map(Cow::Borrowed)?,
                Ok(26) => msg.bounds_list.push(r.read_message::<sytepb::common::Bounds>(bytes)?),
                Ok(34) => msg.account_id = Some(r.read_string(bytes).map(Cow::Borrowed)?),
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl<'a> MessageWrite for VectorizeRequest<'a> {
    fn get_size(&self) -> usize {
        0
        + 1 + sizeof_len((&self.request_id).len())
        + 1 + sizeof_len((&self.image_bin).len())
        + self.bounds_list.iter().map(|s| 1 + sizeof_len((s).get_size())).sum::<usize>()
        + self.account_id.as_ref().map_or(0, |m| 1 + sizeof_len((m).len()))
    }

    fn write_message<W: WriterBackend>(&self, w: &mut Writer<W>) -> Result<()> {
        w.write_with_tag(10, |w| w.write_string(&**&self.request_id))?;
        w.write_with_tag(18, |w| w.write_bytes(&**&self.image_bin))?;
        for s in &self.bounds_list { w.write_with_tag(26, |w| w.write_message(s))?; }
        if let Some(ref s) = self.account_id { w.write_with_tag(34, |w| w.write_string(&**s))?; }
        Ok(())
    }
}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct VectorizeResponse<'a> {
    pub request_id: Cow<'a, str>,
    pub results: Vec<sytepb::vectorize::CBIRResult<'a>>,
}

impl<'a> MessageRead<'a> for VectorizeResponse<'a> {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(10) => msg.request_id = r.read_string(bytes).map(Cow::Borrowed)?,
                Ok(18) => msg.results.push(r.read_message::<sytepb::vectorize::CBIRResult>(bytes)?),
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl<'a> MessageWrite for VectorizeResponse<'a> {
    fn get_size(&self) -> usize {
        0
        + 1 + sizeof_len((&self.request_id).len())
        + self.results.iter().map(|s| 1 + sizeof_len((s).get_size())).sum::<usize>()
    }

    fn write_message<W: WriterBackend>(&self, w: &mut Writer<W>) -> Result<()> {
        w.write_with_tag(10, |w| w.write_string(&**&self.request_id))?;
        for s in &self.results { w.write_with_tag(18, |w| w.write_message(s))?; }
        Ok(())
    }
}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct CBIRResult<'a> {
    pub vector: Cow<'a, [u8]>,
    pub attributes: Vec<sytepb::common::Attribute<'a>>,
}

impl<'a> MessageRead<'a> for CBIRResult<'a> {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(10) => msg.vector = r.read_bytes(bytes).map(Cow::Borrowed)?,
                Ok(18) => msg.attributes.push(r.read_message::<sytepb::common::Attribute>(bytes)?),
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl<'a> MessageWrite for CBIRResult<'a> {
    fn get_size(&self) -> usize {
        0
        + 1 + sizeof_len((&self.vector).len())
        + self.attributes.iter().map(|s| 1 + sizeof_len((s).get_size())).sum::<usize>()
    }

    fn write_message<W: WriterBackend>(&self, w: &mut Writer<W>) -> Result<()> {
        w.write_with_tag(10, |w| w.write_bytes(&**&self.vector))?;
        for s in &self.attributes { w.write_with_tag(18, |w| w.write_message(s))?; }
        Ok(())
    }
}

