// Automatically generated rust module for 'vmr.proto' file

#![allow(non_snake_case)]
#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(unused_imports)]
#![allow(unknown_lints)]
#![allow(clippy::all)]
#![cfg_attr(rustfmt, rustfmt_skip)]


use std::borrow::Cow;
use std::collections::HashMap;
type KVMap<K, V> = HashMap<K, V>;
use quick_protobuf::{MessageRead, MessageWrite, BytesReader, Writer, WriterBackend, Result};
use quick_protobuf::sizeofs::*;
use super::super::*;

#[derive(Debug, Default, PartialEq, Clone)]
pub struct PingRequest { }

impl<'a> MessageRead<'a> for PingRequest {
    fn from_reader(r: &mut BytesReader, _: &[u8]) -> Result<Self> {
        r.read_to_end();
        Ok(Self::default())
    }
}

impl MessageWrite for PingRequest { }

#[derive(Debug, Default, PartialEq, Clone)]
pub struct PingResponse<'a> {
    pub status: Cow<'a, str>,
}

impl<'a> MessageRead<'a> for PingResponse<'a> {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(10) => msg.status = r.read_string(bytes).map(Cow::Borrowed)?,
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl<'a> MessageWrite for PingResponse<'a> {
    fn get_size(&self) -> usize {
        0
        + if self.status == "" { 0 } else { 1 + sizeof_len((&self.status).len()) }
    }

    fn write_message<W: WriterBackend>(&self, w: &mut Writer<W>) -> Result<()> {
        if self.status != "" { w.write_with_tag(10, |w| w.write_string(&**&self.status))?; }
        Ok(())
    }
}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct InferRequest<'a> {
    pub record: Cow<'a, str>,
    pub images_mapping: KVMap<Cow<'a, str>, Cow<'a, [u8]>>,
    pub flow: Cow<'a, str>,
    pub model: Cow<'a, str>,
}

impl<'a> MessageRead<'a> for InferRequest<'a> {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(18) => msg.record = r.read_string(bytes).map(Cow::Borrowed)?,
                Ok(26) => {
                    let (key, value) = r.read_map(bytes, |r, bytes| Ok(r.read_string(bytes).map(Cow::Borrowed)?), |r, bytes| Ok(r.read_bytes(bytes).map(Cow::Borrowed)?))?;
                    msg.images_mapping.insert(key, value);
                }
                Ok(34) => msg.flow = r.read_string(bytes).map(Cow::Borrowed)?,
                Ok(42) => msg.model = r.read_string(bytes).map(Cow::Borrowed)?,
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl<'a> MessageWrite for InferRequest<'a> {
    fn get_size(&self) -> usize {
        0
        + if self.record == "" { 0 } else { 1 + sizeof_len((&self.record).len()) }
        + self.images_mapping.iter().map(|(k, v)| 1 + sizeof_len(2 + sizeof_len((k).len()) + sizeof_len((v).len()))).sum::<usize>()
        + if self.flow == "" { 0 } else { 1 + sizeof_len((&self.flow).len()) }
        + if self.model == "" { 0 } else { 1 + sizeof_len((&self.model).len()) }
    }

    fn write_message<W: WriterBackend>(&self, w: &mut Writer<W>) -> Result<()> {
        if self.record != "" { w.write_with_tag(18, |w| w.write_string(&**&self.record))?; }
        for (k, v) in self.images_mapping.iter() { w.write_with_tag(26, |w| w.write_map(2 + sizeof_len((k).len()) + sizeof_len((v).len()), 10, |w| w.write_string(&**k), 18, |w| w.write_bytes(&**v)))?; }
        if self.flow != "" { w.write_with_tag(34, |w| w.write_string(&**&self.flow))?; }
        if self.model != "" { w.write_with_tag(42, |w| w.write_string(&**&self.model))?; }
        Ok(())
    }
}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct InferResponse<'a> {
    pub predictions: Vec<sytepb::vmr::InferPrediction<'a>>,
}

impl<'a> MessageRead<'a> for InferResponse<'a> {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(34) => msg.predictions.push(r.read_message::<sytepb::vmr::InferPrediction>(bytes)?),
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl<'a> MessageWrite for InferResponse<'a> {
    fn get_size(&self) -> usize {
        0
        + self.predictions.iter().map(|s| 1 + sizeof_len((s).get_size())).sum::<usize>()
    }

    fn write_message<W: WriterBackend>(&self, w: &mut Writer<W>) -> Result<()> {
        for s in &self.predictions { w.write_with_tag(34, |w| w.write_message(s))?; }
        Ok(())
    }
}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct InferPrediction<'a> {
    pub infer: Cow<'a, str>,
    pub vector: Cow<'a, [u8]>,
    pub durations: KVMap<Cow<'a, str>, i32>,
}

impl<'a> MessageRead<'a> for InferPrediction<'a> {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(10) => msg.infer = r.read_string(bytes).map(Cow::Borrowed)?,
                Ok(18) => msg.vector = r.read_bytes(bytes).map(Cow::Borrowed)?,
                Ok(26) => {
                    let (key, value) = r.read_map(bytes, |r, bytes| Ok(r.read_string(bytes).map(Cow::Borrowed)?), |r, bytes| Ok(r.read_int32(bytes)?))?;
                    msg.durations.insert(key, value);
                }
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl<'a> MessageWrite for InferPrediction<'a> {
    fn get_size(&self) -> usize {
        0
        + if self.infer == "" { 0 } else { 1 + sizeof_len((&self.infer).len()) }
        + if self.vector == Cow::Borrowed(b"") { 0 } else { 1 + sizeof_len((&self.vector).len()) }
        + self.durations.iter().map(|(k, v)| 1 + sizeof_len(2 + sizeof_len((k).len()) + sizeof_varint(*(v) as u64))).sum::<usize>()
    }

    fn write_message<W: WriterBackend>(&self, w: &mut Writer<W>) -> Result<()> {
        if self.infer != "" { w.write_with_tag(10, |w| w.write_string(&**&self.infer))?; }
        if self.vector != Cow::Borrowed(b"") { w.write_with_tag(18, |w| w.write_bytes(&**&self.vector))?; }
        for (k, v) in self.durations.iter() { w.write_with_tag(26, |w| w.write_map(2 + sizeof_len((k).len()) + sizeof_varint(*(v) as u64), 10, |w| w.write_string(&**k), 16, |w| w.write_int32(*v)))?; }
        Ok(())
    }
}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct YoloRequest<'a> {
    pub image: Cow<'a, [u8]>,
}

impl<'a> MessageRead<'a> for YoloRequest<'a> {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(10) => msg.image = r.read_bytes(bytes).map(Cow::Borrowed)?,
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl<'a> MessageWrite for YoloRequest<'a> {
    fn get_size(&self) -> usize {
        0
        + if self.image == Cow::Borrowed(b"") { 0 } else { 1 + sizeof_len((&self.image).len()) }
    }

    fn write_message<W: WriterBackend>(&self, w: &mut Writer<W>) -> Result<()> {
        if self.image != Cow::Borrowed(b"") { w.write_with_tag(10, |w| w.write_bytes(&**&self.image))?; }
        Ok(())
    }
}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct YoloResponse<'a> {
    pub result: Cow<'a, str>,
}

impl<'a> MessageRead<'a> for YoloResponse<'a> {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(10) => msg.result = r.read_string(bytes).map(Cow::Borrowed)?,
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl<'a> MessageWrite for YoloResponse<'a> {
    fn get_size(&self) -> usize {
        0
        + if self.result == "" { 0 } else { 1 + sizeof_len((&self.result).len()) }
    }

    fn write_message<W: WriterBackend>(&self, w: &mut Writer<W>) -> Result<()> {
        if self.result != "" { w.write_with_tag(10, |w| w.write_string(&**&self.result))?; }
        Ok(())
    }
}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct CbirRequest<'a> {
    pub image: Cow<'a, [u8]>,
    pub additional_info: Cow<'a, str>,
}

impl<'a> MessageRead<'a> for CbirRequest<'a> {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(10) => msg.image = r.read_bytes(bytes).map(Cow::Borrowed)?,
                Ok(18) => msg.additional_info = r.read_string(bytes).map(Cow::Borrowed)?,
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl<'a> MessageWrite for CbirRequest<'a> {
    fn get_size(&self) -> usize {
        0
        + if self.image == Cow::Borrowed(b"") { 0 } else { 1 + sizeof_len((&self.image).len()) }
        + if self.additional_info == "" { 0 } else { 1 + sizeof_len((&self.additional_info).len()) }
    }

    fn write_message<W: WriterBackend>(&self, w: &mut Writer<W>) -> Result<()> {
        if self.image != Cow::Borrowed(b"") { w.write_with_tag(10, |w| w.write_bytes(&**&self.image))?; }
        if self.additional_info != "" { w.write_with_tag(18, |w| w.write_string(&**&self.additional_info))?; }
        Ok(())
    }
}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct CbirResponse<'a> {
    pub predictions: Vec<sytepb::vmr::CbirPrediction<'a>>,
}

impl<'a> MessageRead<'a> for CbirResponse<'a> {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(10) => msg.predictions.push(r.read_message::<sytepb::vmr::CbirPrediction>(bytes)?),
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl<'a> MessageWrite for CbirResponse<'a> {
    fn get_size(&self) -> usize {
        0
        + self.predictions.iter().map(|s| 1 + sizeof_len((s).get_size())).sum::<usize>()
    }

    fn write_message<W: WriterBackend>(&self, w: &mut Writer<W>) -> Result<()> {
        for s in &self.predictions { w.write_with_tag(10, |w| w.write_message(s))?; }
        Ok(())
    }
}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct CbirPrediction<'a> {
    pub category: Cow<'a, str>,
    pub signatureVector: Cow<'a, [f32]>,
    pub colorVector: Cow<'a, [f32]>,
    pub deeptags: Cow<'a, str>,
    pub similarityGroup: Vec<Cow<'a, str>>,
}

impl<'a> MessageRead<'a> for CbirPrediction<'a> {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(10) => msg.category = r.read_string(bytes).map(Cow::Borrowed)?,
                Ok(18) => msg.signatureVector = r.read_packed_fixed(bytes)?.into(),
                Ok(26) => msg.colorVector = r.read_packed_fixed(bytes)?.into(),
                Ok(34) => msg.deeptags = r.read_string(bytes).map(Cow::Borrowed)?,
                Ok(42) => msg.similarityGroup.push(r.read_string(bytes).map(Cow::Borrowed)?),
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl<'a> MessageWrite for CbirPrediction<'a> {
    fn get_size(&self) -> usize {
        0
        + if self.category == "" { 0 } else { 1 + sizeof_len((&self.category).len()) }
        + if self.signatureVector.is_empty() { 0 } else { 1 + sizeof_len(self.signatureVector.len() * 4) }
        + if self.colorVector.is_empty() { 0 } else { 1 + sizeof_len(self.colorVector.len() * 4) }
        + if self.deeptags == "" { 0 } else { 1 + sizeof_len((&self.deeptags).len()) }
        + self.similarityGroup.iter().map(|s| 1 + sizeof_len((s).len())).sum::<usize>()
    }

    fn write_message<W: WriterBackend>(&self, w: &mut Writer<W>) -> Result<()> {
        if self.category != "" { w.write_with_tag(10, |w| w.write_string(&**&self.category))?; }
        w.write_packed_fixed_with_tag(18, &self.signatureVector)?;
        w.write_packed_fixed_with_tag(26, &self.colorVector)?;
        if self.deeptags != "" { w.write_with_tag(34, |w| w.write_string(&**&self.deeptags))?; }
        for s in &self.similarityGroup { w.write_with_tag(42, |w| w.write_string(&**s))?; }
        Ok(())
    }
}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct TagitRequest<'a> {
    pub data: Cow<'a, str>,
}

impl<'a> MessageRead<'a> for TagitRequest<'a> {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(10) => msg.data = r.read_string(bytes).map(Cow::Borrowed)?,
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl<'a> MessageWrite for TagitRequest<'a> {
    fn get_size(&self) -> usize {
        0
        + if self.data == "" { 0 } else { 1 + sizeof_len((&self.data).len()) }
    }

    fn write_message<W: WriterBackend>(&self, w: &mut Writer<W>) -> Result<()> {
        if self.data != "" { w.write_with_tag(10, |w| w.write_string(&**&self.data))?; }
        Ok(())
    }
}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct TagitResponse<'a> {
    pub result: Cow<'a, str>,
}

impl<'a> MessageRead<'a> for TagitResponse<'a> {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(10) => msg.result = r.read_string(bytes).map(Cow::Borrowed)?,
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl<'a> MessageWrite for TagitResponse<'a> {
    fn get_size(&self) -> usize {
        0
        + if self.result == "" { 0 } else { 1 + sizeof_len((&self.result).len()) }
    }

    fn write_message<W: WriterBackend>(&self, w: &mut Writer<W>) -> Result<()> {
        if self.result != "" { w.write_with_tag(10, |w| w.write_string(&**&self.result))?; }
        Ok(())
    }
}


