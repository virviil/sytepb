// Automatically generated rust module for 'common.proto' file

#![allow(non_snake_case)]
#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(unused_imports)]
#![allow(unknown_lints)]
#![allow(clippy::all)]
#![cfg_attr(rustfmt, rustfmt_skip)]


use std::borrow::Cow;
use quick_protobuf::{MessageRead, MessageWrite, BytesReader, Writer, WriterBackend, Result};
use quick_protobuf::sizeofs::*;
use super::super::*;

#[derive(Debug, Default, PartialEq, Clone)]
pub struct Vector<'a> {
    pub signature: Cow<'a, [f32]>,
    pub color: Cow<'a, [f32]>,
    pub network_version: Option<i32>,
}

impl<'a> MessageRead<'a> for Vector<'a> {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(10) => msg.signature = r.read_packed_fixed(bytes)?.into(),
                Ok(18) => msg.color = r.read_packed_fixed(bytes)?.into(),
                Ok(24) => msg.network_version = Some(r.read_int32(bytes)?),
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl<'a> MessageWrite for Vector<'a> {
    fn get_size(&self) -> usize {
        0
        + if self.signature.is_empty() { 0 } else { 1 + sizeof_len(self.signature.len() * 4) }
        + if self.color.is_empty() { 0 } else { 1 + sizeof_len(self.color.len() * 4) }
        + self.network_version.as_ref().map_or(0, |m| 1 + sizeof_varint(*(m) as u64))
    }

    fn write_message<W: WriterBackend>(&self, w: &mut Writer<W>) -> Result<()> {
        w.write_packed_fixed_with_tag(10, &self.signature)?;
        w.write_packed_fixed_with_tag(18, &self.color)?;
        if let Some(ref s) = self.network_version { w.write_with_tag(24, |w| w.write_int32(*s))?; }
        Ok(())
    }
}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct Attribute<'a> {
    pub name: Cow<'a, str>,
    pub value: Cow<'a, str>,
    pub name_prob: f32,
    pub value_prob: f32,
}

impl<'a> MessageRead<'a> for Attribute<'a> {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(10) => msg.name = r.read_string(bytes).map(Cow::Borrowed)?,
                Ok(18) => msg.value = r.read_string(bytes).map(Cow::Borrowed)?,
                Ok(29) => msg.name_prob = r.read_float(bytes)?,
                Ok(37) => msg.value_prob = r.read_float(bytes)?,
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl<'a> MessageWrite for Attribute<'a> {
    fn get_size(&self) -> usize {
        0
        + 1 + sizeof_len((&self.name).len())
        + 1 + sizeof_len((&self.value).len())
        + 1 + 4
        + 1 + 4
    }

    fn write_message<W: WriterBackend>(&self, w: &mut Writer<W>) -> Result<()> {
        w.write_with_tag(10, |w| w.write_string(&**&self.name))?;
        w.write_with_tag(18, |w| w.write_string(&**&self.value))?;
        w.write_with_tag(29, |w| w.write_float(*&self.name_prob))?;
        w.write_with_tag(37, |w| w.write_float(*&self.value_prob))?;
        Ok(())
    }
}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct Bounds<'a> {
    pub x0: f32,
    pub y0: f32,
    pub x1: f32,
    pub y1: f32,
    pub label: Option<Cow<'a, str>>,
}

impl<'a> MessageRead<'a> for Bounds<'a> {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(13) => msg.x0 = r.read_float(bytes)?,
                Ok(21) => msg.y0 = r.read_float(bytes)?,
                Ok(29) => msg.x1 = r.read_float(bytes)?,
                Ok(37) => msg.y1 = r.read_float(bytes)?,
                Ok(42) => msg.label = Some(r.read_string(bytes).map(Cow::Borrowed)?),
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl<'a> MessageWrite for Bounds<'a> {
    fn get_size(&self) -> usize {
        0
        + 1 + 4
        + 1 + 4
        + 1 + 4
        + 1 + 4
        + self.label.as_ref().map_or(0, |m| 1 + sizeof_len((m).len()))
    }

    fn write_message<W: WriterBackend>(&self, w: &mut Writer<W>) -> Result<()> {
        w.write_with_tag(13, |w| w.write_float(*&self.x0))?;
        w.write_with_tag(21, |w| w.write_float(*&self.y0))?;
        w.write_with_tag(29, |w| w.write_float(*&self.x1))?;
        w.write_with_tag(37, |w| w.write_float(*&self.y1))?;
        if let Some(ref s) = self.label { w.write_with_tag(42, |w| w.write_string(&**s))?; }
        Ok(())
    }
}

