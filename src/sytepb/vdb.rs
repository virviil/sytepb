// Automatically generated rust module for 'vdb.proto' file

#![allow(non_snake_case)]
#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(unused_imports)]
#![allow(unknown_lints)]
#![allow(clippy::all)]
#![cfg_attr(rustfmt, rustfmt_skip)]


use std::borrow::Cow;
use quick_protobuf::{MessageRead, MessageWrite, BytesReader, Writer, WriterBackend, Result};
use quick_protobuf::sizeofs::*;
use super::super::*;

#[derive(Debug, Default, PartialEq, Clone)]
pub struct VectorIdentity<'a> {
    pub id: Cow<'a, str>,
    pub index: Option<Cow<'a, str>>,
}

impl<'a> MessageRead<'a> for VectorIdentity<'a> {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(10) => msg.id = r.read_string(bytes).map(Cow::Borrowed)?,
                Ok(18) => msg.index = Some(r.read_string(bytes).map(Cow::Borrowed)?),
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl<'a> MessageWrite for VectorIdentity<'a> {
    fn get_size(&self) -> usize {
        0
        + 1 + sizeof_len((&self.id).len())
        + self.index.as_ref().map_or(0, |m| 1 + sizeof_len((m).len()))
    }

    fn write_message<W: WriterBackend>(&self, w: &mut Writer<W>) -> Result<()> {
        w.write_with_tag(10, |w| w.write_string(&**&self.id))?;
        if let Some(ref s) = self.index { w.write_with_tag(18, |w| w.write_string(&**s))?; }
        Ok(())
    }
}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct Vector<'a> {
    pub identity: sytepb::vdb::VectorIdentity<'a>,
    pub vector: Cow<'a, [u8]>,
}

impl<'a> MessageRead<'a> for Vector<'a> {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(10) => msg.identity = r.read_message::<sytepb::vdb::VectorIdentity>(bytes)?,
                Ok(18) => msg.vector = r.read_bytes(bytes).map(Cow::Borrowed)?,
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl<'a> MessageWrite for Vector<'a> {
    fn get_size(&self) -> usize {
        0
        + 1 + sizeof_len((&self.identity).get_size())
        + 1 + sizeof_len((&self.vector).len())
    }

    fn write_message<W: WriterBackend>(&self, w: &mut Writer<W>) -> Result<()> {
        w.write_with_tag(10, |w| w.write_message(&self.identity))?;
        w.write_with_tag(18, |w| w.write_bytes(&**&self.vector))?;
        Ok(())
    }
}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct VectorCreationStatus<'a> {
    pub identity: sytepb::vdb::VectorIdentity<'a>,
    pub status: Cow<'a, str>,
    pub metadata: Option<Cow<'a, [u8]>>,
}

impl<'a> MessageRead<'a> for VectorCreationStatus<'a> {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(10) => msg.identity = r.read_message::<sytepb::vdb::VectorIdentity>(bytes)?,
                Ok(18) => msg.status = r.read_string(bytes).map(Cow::Borrowed)?,
                Ok(26) => msg.metadata = Some(r.read_bytes(bytes).map(Cow::Borrowed)?),
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl<'a> MessageWrite for VectorCreationStatus<'a> {
    fn get_size(&self) -> usize {
        0
        + 1 + sizeof_len((&self.identity).get_size())
        + 1 + sizeof_len((&self.status).len())
        + self.metadata.as_ref().map_or(0, |m| 1 + sizeof_len((m).len()))
    }

    fn write_message<W: WriterBackend>(&self, w: &mut Writer<W>) -> Result<()> {
        w.write_with_tag(10, |w| w.write_message(&self.identity))?;
        w.write_with_tag(18, |w| w.write_string(&**&self.status))?;
        if let Some(ref s) = self.metadata { w.write_with_tag(26, |w| w.write_bytes(&**s))?; }
        Ok(())
    }
}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct BulkCreateVectorsRequest<'a> {
    pub vectors: Vec<sytepb::vdb::Vector<'a>>,
}

impl<'a> MessageRead<'a> for BulkCreateVectorsRequest<'a> {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(10) => msg.vectors.push(r.read_message::<sytepb::vdb::Vector>(bytes)?),
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl<'a> MessageWrite for BulkCreateVectorsRequest<'a> {
    fn get_size(&self) -> usize {
        0
        + self.vectors.iter().map(|s| 1 + sizeof_len((s).get_size())).sum::<usize>()
    }

    fn write_message<W: WriterBackend>(&self, w: &mut Writer<W>) -> Result<()> {
        for s in &self.vectors { w.write_with_tag(10, |w| w.write_message(s))?; }
        Ok(())
    }
}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct BulkCreateVectorsResponse<'a> {
    pub statuses: Vec<sytepb::vdb::VectorCreationStatus<'a>>,
}

impl<'a> MessageRead<'a> for BulkCreateVectorsResponse<'a> {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(10) => msg.statuses.push(r.read_message::<sytepb::vdb::VectorCreationStatus>(bytes)?),
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl<'a> MessageWrite for BulkCreateVectorsResponse<'a> {
    fn get_size(&self) -> usize {
        0
        + self.statuses.iter().map(|s| 1 + sizeof_len((s).get_size())).sum::<usize>()
    }

    fn write_message<W: WriterBackend>(&self, w: &mut Writer<W>) -> Result<()> {
        for s in &self.statuses { w.write_with_tag(10, |w| w.write_message(s))?; }
        Ok(())
    }
}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct BulkGetVectorsRequest<'a> {
    pub identities: Vec<sytepb::vdb::VectorIdentity<'a>>,
}

impl<'a> MessageRead<'a> for BulkGetVectorsRequest<'a> {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(10) => msg.identities.push(r.read_message::<sytepb::vdb::VectorIdentity>(bytes)?),
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl<'a> MessageWrite for BulkGetVectorsRequest<'a> {
    fn get_size(&self) -> usize {
        0
        + self.identities.iter().map(|s| 1 + sizeof_len((s).get_size())).sum::<usize>()
    }

    fn write_message<W: WriterBackend>(&self, w: &mut Writer<W>) -> Result<()> {
        for s in &self.identities { w.write_with_tag(10, |w| w.write_message(s))?; }
        Ok(())
    }
}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct BulkGetVectorsResponse<'a> {
    pub vectors: Vec<sytepb::vdb::Vector<'a>>,
}

impl<'a> MessageRead<'a> for BulkGetVectorsResponse<'a> {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(10) => msg.vectors.push(r.read_message::<sytepb::vdb::Vector>(bytes)?),
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl<'a> MessageWrite for BulkGetVectorsResponse<'a> {
    fn get_size(&self) -> usize {
        0
        + self.vectors.iter().map(|s| 1 + sizeof_len((s).get_size())).sum::<usize>()
    }

    fn write_message<W: WriterBackend>(&self, w: &mut Writer<W>) -> Result<()> {
        for s in &self.vectors { w.write_with_tag(10, |w| w.write_message(s))?; }
        Ok(())
    }
}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct DistanceQuery<'a> {
    pub vector: Cow<'a, [u8]>,
    pub vector_identities: Vec<sytepb::vdb::VectorIdentity<'a>>,
    pub color_weight: Option<f32>,
}

impl<'a> MessageRead<'a> for DistanceQuery<'a> {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(10) => msg.vector = r.read_bytes(bytes).map(Cow::Borrowed)?,
                Ok(18) => msg.vector_identities.push(r.read_message::<sytepb::vdb::VectorIdentity>(bytes)?),
                Ok(29) => msg.color_weight = Some(r.read_float(bytes)?),
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl<'a> MessageWrite for DistanceQuery<'a> {
    fn get_size(&self) -> usize {
        0
        + 1 + sizeof_len((&self.vector).len())
        + self.vector_identities.iter().map(|s| 1 + sizeof_len((s).get_size())).sum::<usize>()
        + self.color_weight.as_ref().map_or(0, |_| 1 + 4)
    }

    fn write_message<W: WriterBackend>(&self, w: &mut Writer<W>) -> Result<()> {
        w.write_with_tag(10, |w| w.write_bytes(&**&self.vector))?;
        for s in &self.vector_identities { w.write_with_tag(18, |w| w.write_message(s))?; }
        if let Some(ref s) = self.color_weight { w.write_with_tag(29, |w| w.write_float(*s))?; }
        Ok(())
    }
}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct RelativeDistance<'a> {
    pub identity: sytepb::vdb::VectorIdentity<'a>,
    pub distance: f32,
}

impl<'a> MessageRead<'a> for RelativeDistance<'a> {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(10) => msg.identity = r.read_message::<sytepb::vdb::VectorIdentity>(bytes)?,
                Ok(21) => msg.distance = r.read_float(bytes)?,
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl<'a> MessageWrite for RelativeDistance<'a> {
    fn get_size(&self) -> usize {
        0
        + 1 + sizeof_len((&self.identity).get_size())
        + 1 + 4
    }

    fn write_message<W: WriterBackend>(&self, w: &mut Writer<W>) -> Result<()> {
        w.write_with_tag(10, |w| w.write_message(&self.identity))?;
        w.write_with_tag(21, |w| w.write_float(*&self.distance))?;
        Ok(())
    }
}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct DistanceReply<'a> {
    pub relative_distances: Vec<sytepb::vdb::RelativeDistance<'a>>,
}

impl<'a> MessageRead<'a> for DistanceReply<'a> {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(10) => msg.relative_distances.push(r.read_message::<sytepb::vdb::RelativeDistance>(bytes)?),
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl<'a> MessageWrite for DistanceReply<'a> {
    fn get_size(&self) -> usize {
        0
        + self.relative_distances.iter().map(|s| 1 + sizeof_len((s).get_size())).sum::<usize>()
    }

    fn write_message<W: WriterBackend>(&self, w: &mut Writer<W>) -> Result<()> {
        for s in &self.relative_distances { w.write_with_tag(10, |w| w.write_message(s))?; }
        Ok(())
    }
}

