// Automatically generated rust module for 'ads.proto' file

#![allow(non_snake_case)]
#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(unused_imports)]
#![allow(unknown_lints)]
#![allow(clippy::all)]
#![cfg_attr(rustfmt, rustfmt_skip)]


use std::borrow::Cow;
use quick_protobuf::{MessageRead, MessageWrite, BytesReader, Writer, WriterBackend, Result};
use quick_protobuf::sizeofs::*;
use super::super::*;

#[derive(Debug, Default, PartialEq, Clone)]
pub struct Ad<'a> {
    pub id: Option<Cow<'a, str>>,
    pub index_name: Option<Cow<'a, str>>,
    pub vector: Cow<'a, [u8]>,
    pub attributes: Vec<sytepb::common::Attribute<'a>>,
    pub sig_md5: Option<Cow<'a, str>>,
    pub csv_id: Option<Cow<'a, str>>,
    pub vector_id: Option<Cow<'a, str>>,
    pub metadata: Option<Cow<'a, [u8]>>,
    pub cluster_id: Option<Cow<'a, str>>,
}

impl<'a> MessageRead<'a> for Ad<'a> {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(10) => msg.id = Some(r.read_string(bytes).map(Cow::Borrowed)?),
                Ok(18) => msg.index_name = Some(r.read_string(bytes).map(Cow::Borrowed)?),
                Ok(26) => msg.vector = r.read_bytes(bytes).map(Cow::Borrowed)?,
                Ok(34) => msg.attributes.push(r.read_message::<sytepb::common::Attribute>(bytes)?),
                Ok(42) => msg.sig_md5 = Some(r.read_string(bytes).map(Cow::Borrowed)?),
                Ok(50) => msg.csv_id = Some(r.read_string(bytes).map(Cow::Borrowed)?),
                Ok(58) => msg.vector_id = Some(r.read_string(bytes).map(Cow::Borrowed)?),
                Ok(66) => msg.metadata = Some(r.read_bytes(bytes).map(Cow::Borrowed)?),
                Ok(74) => msg.cluster_id = Some(r.read_string(bytes).map(Cow::Borrowed)?),
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl<'a> MessageWrite for Ad<'a> {
    fn get_size(&self) -> usize {
        0
        + self.id.as_ref().map_or(0, |m| 1 + sizeof_len((m).len()))
        + self.index_name.as_ref().map_or(0, |m| 1 + sizeof_len((m).len()))
        + 1 + sizeof_len((&self.vector).len())
        + self.attributes.iter().map(|s| 1 + sizeof_len((s).get_size())).sum::<usize>()
        + self.sig_md5.as_ref().map_or(0, |m| 1 + sizeof_len((m).len()))
        + self.csv_id.as_ref().map_or(0, |m| 1 + sizeof_len((m).len()))
        + self.vector_id.as_ref().map_or(0, |m| 1 + sizeof_len((m).len()))
        + self.metadata.as_ref().map_or(0, |m| 1 + sizeof_len((m).len()))
        + self.cluster_id.as_ref().map_or(0, |m| 1 + sizeof_len((m).len()))
    }

    fn write_message<W: WriterBackend>(&self, w: &mut Writer<W>) -> Result<()> {
        if let Some(ref s) = self.id { w.write_with_tag(10, |w| w.write_string(&**s))?; }
        if let Some(ref s) = self.index_name { w.write_with_tag(18, |w| w.write_string(&**s))?; }
        w.write_with_tag(26, |w| w.write_bytes(&**&self.vector))?;
        for s in &self.attributes { w.write_with_tag(34, |w| w.write_message(s))?; }
        if let Some(ref s) = self.sig_md5 { w.write_with_tag(42, |w| w.write_string(&**s))?; }
        if let Some(ref s) = self.csv_id { w.write_with_tag(50, |w| w.write_string(&**s))?; }
        if let Some(ref s) = self.vector_id { w.write_with_tag(58, |w| w.write_string(&**s))?; }
        if let Some(ref s) = self.metadata { w.write_with_tag(66, |w| w.write_bytes(&**s))?; }
        if let Some(ref s) = self.cluster_id { w.write_with_tag(74, |w| w.write_string(&**s))?; }
        Ok(())
    }
}

