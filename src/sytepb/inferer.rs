// Automatically generated rust module for 'inferer.proto' file

#![allow(non_snake_case)]
#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(unused_imports)]
#![allow(unknown_lints)]
#![allow(clippy::all)]
#![cfg_attr(rustfmt, rustfmt_skip)]


use std::borrow::Cow;
use std::collections::HashMap;
type KVMap<K, V> = HashMap<K, V>;
use quick_protobuf::{MessageRead, MessageWrite, BytesReader, Writer, WriterBackend, Result};
use quick_protobuf::sizeofs::*;
use super::super::*;

#[derive(Debug, Default, PartialEq, Clone)]
pub struct PingRequest { }

impl<'a> MessageRead<'a> for PingRequest {
    fn from_reader(r: &mut BytesReader, _: &[u8]) -> Result<Self> {
        r.read_to_end();
        Ok(Self::default())
    }
}

impl MessageWrite for PingRequest { }

#[derive(Debug, Default, PartialEq, Clone)]
pub struct PingResponse<'a> {
    pub health: Cow<'a, str>,
    pub details: Cow<'a, [u8]>,
}

impl<'a> MessageRead<'a> for PingResponse<'a> {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(10) => msg.health = r.read_string(bytes).map(Cow::Borrowed)?,
                Ok(18) => msg.details = r.read_bytes(bytes).map(Cow::Borrowed)?,
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl<'a> MessageWrite for PingResponse<'a> {
    fn get_size(&self) -> usize {
        0
        + if self.health == "" { 0 } else { 1 + sizeof_len((&self.health).len()) }
        + if self.details == Cow::Borrowed(b"") { 0 } else { 1 + sizeof_len((&self.details).len()) }
    }

    fn write_message<W: WriterBackend>(&self, w: &mut Writer<W>) -> Result<()> {
        if self.health != "" { w.write_with_tag(10, |w| w.write_string(&**&self.health))?; }
        if self.details != Cow::Borrowed(b"") { w.write_with_tag(18, |w| w.write_bytes(&**&self.details))?; }
        Ok(())
    }
}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct InferRequest<'a> {
    pub model_name: Cow<'a, str>,
    pub model_version: Cow<'a, str>,
    pub input: Option<sytepb::inferer::Input<'a>>,
    pub options: Cow<'a, [u8]>,
}

impl<'a> MessageRead<'a> for InferRequest<'a> {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(10) => msg.model_name = r.read_string(bytes).map(Cow::Borrowed)?,
                Ok(18) => msg.model_version = r.read_string(bytes).map(Cow::Borrowed)?,
                Ok(26) => msg.input = Some(r.read_message::<sytepb::inferer::Input>(bytes)?),
                Ok(34) => msg.options = r.read_bytes(bytes).map(Cow::Borrowed)?,
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl<'a> MessageWrite for InferRequest<'a> {
    fn get_size(&self) -> usize {
        0
        + if self.model_name == "" { 0 } else { 1 + sizeof_len((&self.model_name).len()) }
        + if self.model_version == "" { 0 } else { 1 + sizeof_len((&self.model_version).len()) }
        + self.input.as_ref().map_or(0, |m| 1 + sizeof_len((m).get_size()))
        + if self.options == Cow::Borrowed(b"") { 0 } else { 1 + sizeof_len((&self.options).len()) }
    }

    fn write_message<W: WriterBackend>(&self, w: &mut Writer<W>) -> Result<()> {
        if self.model_name != "" { w.write_with_tag(10, |w| w.write_string(&**&self.model_name))?; }
        if self.model_version != "" { w.write_with_tag(18, |w| w.write_string(&**&self.model_version))?; }
        if let Some(ref s) = self.input { w.write_with_tag(26, |w| w.write_message(s))?; }
        if self.options != Cow::Borrowed(b"") { w.write_with_tag(34, |w| w.write_bytes(&**&self.options))?; }
        Ok(())
    }
}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct Input<'a> {
    pub entities: Vec<sytepb::inferer::InputEntity<'a>>,
    pub options: Cow<'a, [u8]>,
}

impl<'a> MessageRead<'a> for Input<'a> {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(10) => msg.entities.push(r.read_message::<sytepb::inferer::InputEntity>(bytes)?),
                Ok(18) => msg.options = r.read_bytes(bytes).map(Cow::Borrowed)?,
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl<'a> MessageWrite for Input<'a> {
    fn get_size(&self) -> usize {
        0
        + self.entities.iter().map(|s| 1 + sizeof_len((s).get_size())).sum::<usize>()
        + if self.options == Cow::Borrowed(b"") { 0 } else { 1 + sizeof_len((&self.options).len()) }
    }

    fn write_message<W: WriterBackend>(&self, w: &mut Writer<W>) -> Result<()> {
        for s in &self.entities { w.write_with_tag(10, |w| w.write_message(s))?; }
        if self.options != Cow::Borrowed(b"") { w.write_with_tag(18, |w| w.write_bytes(&**&self.options))?; }
        Ok(())
    }
}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct InputEntity<'a> {
    pub options: Cow<'a, [u8]>,
    pub entity: sytepb::inferer::mod_InputEntity::OneOfentity<'a>,
}

impl<'a> MessageRead<'a> for InputEntity<'a> {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(42) => msg.options = r.read_bytes(bytes).map(Cow::Borrowed)?,
                Ok(10) => msg.entity = sytepb::inferer::mod_InputEntity::OneOfentity::listing(r.read_message::<sytepb::inferer::InputListing>(bytes)?),
                Ok(18) => msg.entity = sytepb::inferer::mod_InputEntity::OneOfentity::mapping(r.read_message::<sytepb::inferer::InputMapping>(bytes)?),
                Ok(34) => msg.entity = sytepb::inferer::mod_InputEntity::OneOfentity::data(r.read_bytes(bytes).map(Cow::Borrowed)?),
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl<'a> MessageWrite for InputEntity<'a> {
    fn get_size(&self) -> usize {
        0
        + if self.options == Cow::Borrowed(b"") { 0 } else { 1 + sizeof_len((&self.options).len()) }
        + match self.entity {
            sytepb::inferer::mod_InputEntity::OneOfentity::listing(ref m) => 1 + sizeof_len((m).get_size()),
            sytepb::inferer::mod_InputEntity::OneOfentity::mapping(ref m) => 1 + sizeof_len((m).get_size()),
            sytepb::inferer::mod_InputEntity::OneOfentity::data(ref m) => 1 + sizeof_len((m).len()),
            sytepb::inferer::mod_InputEntity::OneOfentity::None => 0,
    }    }

    fn write_message<W: WriterBackend>(&self, w: &mut Writer<W>) -> Result<()> {
        if self.options != Cow::Borrowed(b"") { w.write_with_tag(42, |w| w.write_bytes(&**&self.options))?; }
        match self.entity {            sytepb::inferer::mod_InputEntity::OneOfentity::listing(ref m) => { w.write_with_tag(10, |w| w.write_message(m))? },
            sytepb::inferer::mod_InputEntity::OneOfentity::mapping(ref m) => { w.write_with_tag(18, |w| w.write_message(m))? },
            sytepb::inferer::mod_InputEntity::OneOfentity::data(ref m) => { w.write_with_tag(34, |w| w.write_bytes(&**m))? },
            sytepb::inferer::mod_InputEntity::OneOfentity::None => {},
    }        Ok(())
    }
}

pub mod mod_InputEntity {

use super::*;

#[derive(Debug, PartialEq, Clone)]
pub enum OneOfentity<'a> {
    listing(sytepb::inferer::InputListing<'a>),
    mapping(sytepb::inferer::InputMapping<'a>),
    data(Cow<'a, [u8]>),
    None,
}

impl<'a> Default for OneOfentity<'a> {
    fn default() -> Self {
        OneOfentity::None
    }
}

}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct InputMapping<'a> {
    pub data: KVMap<Cow<'a, str>, sytepb::inferer::InputEntity<'a>>,
}

impl<'a> MessageRead<'a> for InputMapping<'a> {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(10) => {
                    let (key, value) = r.read_map(bytes, |r, bytes| Ok(r.read_string(bytes).map(Cow::Borrowed)?), |r, bytes| Ok(r.read_message::<sytepb::inferer::InputEntity>(bytes)?))?;
                    msg.data.insert(key, value);
                }
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl<'a> MessageWrite for InputMapping<'a> {
    fn get_size(&self) -> usize {
        0
        + self.data.iter().map(|(k, v)| 1 + sizeof_len(2 + sizeof_len((k).len()) + sizeof_len((v).get_size()))).sum::<usize>()
    }

    fn write_message<W: WriterBackend>(&self, w: &mut Writer<W>) -> Result<()> {
        for (k, v) in self.data.iter() { w.write_with_tag(10, |w| w.write_map(2 + sizeof_len((k).len()) + sizeof_len((v).get_size()), 10, |w| w.write_string(&**k), 18, |w| w.write_message(v)))?; }
        Ok(())
    }
}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct InputListing<'a> {
    pub data: Vec<sytepb::inferer::InputEntity<'a>>,
}

impl<'a> MessageRead<'a> for InputListing<'a> {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(10) => msg.data.push(r.read_message::<sytepb::inferer::InputEntity>(bytes)?),
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl<'a> MessageWrite for InputListing<'a> {
    fn get_size(&self) -> usize {
        0
        + self.data.iter().map(|s| 1 + sizeof_len((s).get_size())).sum::<usize>()
    }

    fn write_message<W: WriterBackend>(&self, w: &mut Writer<W>) -> Result<()> {
        for s in &self.data { w.write_with_tag(10, |w| w.write_message(s))?; }
        Ok(())
    }
}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct InferResponse<'a> {
    pub prediction: Option<sytepb::inferer::Prediction<'a>>,
    pub details: Cow<'a, [u8]>,
}

impl<'a> MessageRead<'a> for InferResponse<'a> {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(10) => msg.prediction = Some(r.read_message::<sytepb::inferer::Prediction>(bytes)?),
                Ok(18) => msg.details = r.read_bytes(bytes).map(Cow::Borrowed)?,
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl<'a> MessageWrite for InferResponse<'a> {
    fn get_size(&self) -> usize {
        0
        + self.prediction.as_ref().map_or(0, |m| 1 + sizeof_len((m).get_size()))
        + if self.details == Cow::Borrowed(b"") { 0 } else { 1 + sizeof_len((&self.details).len()) }
    }

    fn write_message<W: WriterBackend>(&self, w: &mut Writer<W>) -> Result<()> {
        if let Some(ref s) = self.prediction { w.write_with_tag(10, |w| w.write_message(s))?; }
        if self.details != Cow::Borrowed(b"") { w.write_with_tag(18, |w| w.write_bytes(&**&self.details))?; }
        Ok(())
    }
}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct Prediction<'a> {
    pub entities: Vec<sytepb::inferer::PredictionEntity<'a>>,
    pub details: Cow<'a, [u8]>,
}

impl<'a> MessageRead<'a> for Prediction<'a> {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(10) => msg.entities.push(r.read_message::<sytepb::inferer::PredictionEntity>(bytes)?),
                Ok(18) => msg.details = r.read_bytes(bytes).map(Cow::Borrowed)?,
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl<'a> MessageWrite for Prediction<'a> {
    fn get_size(&self) -> usize {
        0
        + self.entities.iter().map(|s| 1 + sizeof_len((s).get_size())).sum::<usize>()
        + if self.details == Cow::Borrowed(b"") { 0 } else { 1 + sizeof_len((&self.details).len()) }
    }

    fn write_message<W: WriterBackend>(&self, w: &mut Writer<W>) -> Result<()> {
        for s in &self.entities { w.write_with_tag(10, |w| w.write_message(s))?; }
        if self.details != Cow::Borrowed(b"") { w.write_with_tag(18, |w| w.write_bytes(&**&self.details))?; }
        Ok(())
    }
}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct PredictionEntity<'a> {
    pub details: Cow<'a, [u8]>,
    pub entity: sytepb::inferer::mod_PredictionEntity::OneOfentity<'a>,
}

impl<'a> MessageRead<'a> for PredictionEntity<'a> {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(42) => msg.details = r.read_bytes(bytes).map(Cow::Borrowed)?,
                Ok(10) => msg.entity = sytepb::inferer::mod_PredictionEntity::OneOfentity::listing(r.read_message::<sytepb::inferer::PredictionListing>(bytes)?),
                Ok(18) => msg.entity = sytepb::inferer::mod_PredictionEntity::OneOfentity::mapping(r.read_message::<sytepb::inferer::PredictionMapping>(bytes)?),
                Ok(34) => msg.entity = sytepb::inferer::mod_PredictionEntity::OneOfentity::data(r.read_bytes(bytes).map(Cow::Borrowed)?),
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl<'a> MessageWrite for PredictionEntity<'a> {
    fn get_size(&self) -> usize {
        0
        + if self.details == Cow::Borrowed(b"") { 0 } else { 1 + sizeof_len((&self.details).len()) }
        + match self.entity {
            sytepb::inferer::mod_PredictionEntity::OneOfentity::listing(ref m) => 1 + sizeof_len((m).get_size()),
            sytepb::inferer::mod_PredictionEntity::OneOfentity::mapping(ref m) => 1 + sizeof_len((m).get_size()),
            sytepb::inferer::mod_PredictionEntity::OneOfentity::data(ref m) => 1 + sizeof_len((m).len()),
            sytepb::inferer::mod_PredictionEntity::OneOfentity::None => 0,
    }    }

    fn write_message<W: WriterBackend>(&self, w: &mut Writer<W>) -> Result<()> {
        if self.details != Cow::Borrowed(b"") { w.write_with_tag(42, |w| w.write_bytes(&**&self.details))?; }
        match self.entity {            sytepb::inferer::mod_PredictionEntity::OneOfentity::listing(ref m) => { w.write_with_tag(10, |w| w.write_message(m))? },
            sytepb::inferer::mod_PredictionEntity::OneOfentity::mapping(ref m) => { w.write_with_tag(18, |w| w.write_message(m))? },
            sytepb::inferer::mod_PredictionEntity::OneOfentity::data(ref m) => { w.write_with_tag(34, |w| w.write_bytes(&**m))? },
            sytepb::inferer::mod_PredictionEntity::OneOfentity::None => {},
    }        Ok(())
    }
}

pub mod mod_PredictionEntity {

use super::*;

#[derive(Debug, PartialEq, Clone)]
pub enum OneOfentity<'a> {
    listing(sytepb::inferer::PredictionListing<'a>),
    mapping(sytepb::inferer::PredictionMapping<'a>),
    data(Cow<'a, [u8]>),
    None,
}

impl<'a> Default for OneOfentity<'a> {
    fn default() -> Self {
        OneOfentity::None
    }
}

}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct PredictionMapping<'a> {
    pub data: KVMap<Cow<'a, str>, sytepb::inferer::PredictionEntity<'a>>,
}

impl<'a> MessageRead<'a> for PredictionMapping<'a> {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(10) => {
                    let (key, value) = r.read_map(bytes, |r, bytes| Ok(r.read_string(bytes).map(Cow::Borrowed)?), |r, bytes| Ok(r.read_message::<sytepb::inferer::PredictionEntity>(bytes)?))?;
                    msg.data.insert(key, value);
                }
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl<'a> MessageWrite for PredictionMapping<'a> {
    fn get_size(&self) -> usize {
        0
        + self.data.iter().map(|(k, v)| 1 + sizeof_len(2 + sizeof_len((k).len()) + sizeof_len((v).get_size()))).sum::<usize>()
    }

    fn write_message<W: WriterBackend>(&self, w: &mut Writer<W>) -> Result<()> {
        for (k, v) in self.data.iter() { w.write_with_tag(10, |w| w.write_map(2 + sizeof_len((k).len()) + sizeof_len((v).get_size()), 10, |w| w.write_string(&**k), 18, |w| w.write_message(v)))?; }
        Ok(())
    }
}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct PredictionListing<'a> {
    pub data: Vec<sytepb::inferer::PredictionEntity<'a>>,
}

impl<'a> MessageRead<'a> for PredictionListing<'a> {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(10) => msg.data.push(r.read_message::<sytepb::inferer::PredictionEntity>(bytes)?),
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl<'a> MessageWrite for PredictionListing<'a> {
    fn get_size(&self) -> usize {
        0
        + self.data.iter().map(|s| 1 + sizeof_len((s).get_size())).sum::<usize>()
    }

    fn write_message<W: WriterBackend>(&self, w: &mut Writer<W>) -> Result<()> {
        for s in &self.data { w.write_with_tag(10, |w| w.write_message(s))?; }
        Ok(())
    }
}


