%% -*- coding: utf-8 -*-
%% Automatically generated, do not edit
%% Generated by gpb_compile version 4.18.0

-ifndef(sytepb_ads).
-define(sytepb_ads, true).

-define(sytepb_ads_gpb_version, "4.18.0").

-ifndef('SYTEPB.ADS.AD_PB_H').
-define('SYTEPB.ADS.AD_PB_H', true).
-record('sytepb.ads.Ad',
        {id                     :: unicode:chardata() | undefined, % = 1, optional
         index_name             :: unicode:chardata() | undefined, % = 2, optional
         vector                 :: iodata() | undefined, % = 3, required
         attributes = []        :: [sytepb_ads:'sytepb.common.Attribute'()] | undefined, % = 4, repeated
         sig_md5                :: unicode:chardata() | undefined, % = 5, optional
         csv_id                 :: unicode:chardata() | undefined, % = 6, optional
         vector_id              :: unicode:chardata() | undefined, % = 7, optional
         metadata               :: iodata() | undefined, % = 8, optional
         cluster_id             :: unicode:chardata() | undefined % = 9, optional
        }).
-endif.

-ifndef('SYTEPB.COMMON.VECTOR_PB_H').
-define('SYTEPB.COMMON.VECTOR_PB_H', true).
-record('sytepb.common.Vector',
        {signature = []         :: [float() | integer() | infinity | '-infinity' | nan] | undefined, % = 1, repeated
         color = []             :: [float() | integer() | infinity | '-infinity' | nan] | undefined, % = 2, repeated
         network_version        :: integer() | undefined % = 3, optional, 32 bits
        }).
-endif.

-ifndef('SYTEPB.COMMON.ATTRIBUTE_PB_H').
-define('SYTEPB.COMMON.ATTRIBUTE_PB_H', true).
-record('sytepb.common.Attribute',
        {name                   :: unicode:chardata() | undefined, % = 1, required
         value                  :: unicode:chardata() | undefined, % = 2, required
         name_prob              :: float() | integer() | infinity | '-infinity' | nan | undefined, % = 3, required
         value_prob             :: float() | integer() | infinity | '-infinity' | nan | undefined % = 4, required
        }).
-endif.

-ifndef('SYTEPB.COMMON.BOUNDS_PB_H').
-define('SYTEPB.COMMON.BOUNDS_PB_H', true).
-record('sytepb.common.Bounds',
        {x0                     :: float() | integer() | infinity | '-infinity' | nan | undefined, % = 1, required
         y0                     :: float() | integer() | infinity | '-infinity' | nan | undefined, % = 2, required
         x1                     :: float() | integer() | infinity | '-infinity' | nan | undefined, % = 3, required
         y1                     :: float() | integer() | infinity | '-infinity' | nan | undefined, % = 4, required
         label                  :: unicode:chardata() | undefined % = 5, optional
        }).
-endif.

-endif.
